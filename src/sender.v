`default_nettype none

module sender #(
   parameter CLOCK_FREQ = 100_000_000,
   parameter NUM_NEOPIXELS = 8,
   parameter NEOPIXEL_ADDR_WIDTH = 3
) (
   output wire	     dout,
   input wire [23:0] rgb[NEOPIXEL_ADDR_WIDTH-1:0],
   input wire	     write_en,
   input wire	     rst,
   input wire	     clk
);

   
endmodule
