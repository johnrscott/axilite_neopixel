// Simple simulation testbench for neopixel PHY module
//
// The PHY module sends 1.25us square pulses with a duty
// cycle that depends on whether a 0 or 1 is being sent.
// The pulses are triggered by setting bit_value and asserting
// send. The output busy is asserted until the bit is sent.
//
// This testbench simulates sending several bits, and checks
// that the output respects the correct period and duty
// cycle. After send is asserted, it checks that busy and
// dout are asserted; then it waits for a duty-period
// depending on whether 0 or 1 is being sent, and checks
// that dout is deasserted. After waiting a full period,
// it is checked that busy is deasserted.
//
// If SUCCESS is printed, all the checks passed. Otherwise,
// ERROR is printed and the simulation is terminated.
// The trace is written to vars.vcd.

`include "assert.v"

module bit_sender_tb();

   // Stop the simulation if it runs too long
   localparam TIMEOUT = 1500;
   
   // Set the clock frequency and calculate neopixel
   // high times for 0 and 1. These are used to make
   // approximate checks of the timing.
   localparam CLOCK_FREQ = 32_000_000;
   localparam NEOPIXEL_PERIOD_COUNT = 40; // 1.25us
   localparam ZERO_HIGH_COUNT = 10; // 0.3us
   localparam ONE_HIGH_COUNT = 20; // 0.6us
   
   reg bit_value, send, clk, rst;
   wire	dout, busy;

   task reset;
     begin
	bit_value = 0;
	send = 0;
	#4 rst = 1;
	#4 rst = 0;
	#4;
     end
   endtask
   
   task send_bit(input reg value);
      begin

	 $display("Sending bit with value %d", value);
        
	 while (busy)
	   #1; // Wait for module to not be busy
	 
	 // Set the value to send
	 bit_value = value;
	 send = 1;
	 #2 send = 0;

	 while (!busy)
	   #1; // Wait for transmission to begin

	 // Check that the initial dout is high
	 `assert(dout == 1, "ERROR: dout != 1 at start of send");
	 
	 // This is a sanity test that sampling the dout line
	 // halfway through the PWM cycle gives the correct logic
	 // level, and that the PWM cycle lasts the correct amount
	 // of time (note that each clock takes #2 in this simulation).
	 // Review the wave traces to check the timing is correct.
	 if (value) begin
	    
	    #NEOPIXEL_PERIOD_COUNT;
	    `assert(dout == 1,"ERROR: sample dout == 0 failed while sending 1");

	    #NEOPIXEL_PERIOD_COUNT;
	    #4 `assert(busy == 0,"ERROR: expected !busy after neopixel period");

	 end
	 else begin
	    
	    #NEOPIXEL_PERIOD_COUNT;
	    `assert(dout == 0,"ERROR: sample dout == 0 failed while sending 0");

	    #NEOPIXEL_PERIOD_COUNT;
	    #4 `assert(busy == 0,"ERROR: expected !busy after neopixel period");

	 end
	    
      end
   endtask
   
   bit_sender #(
      .CLOCK_FREQ(32000000)
   ) dut (
      .dout(dout),
      .busy(busy),
      .bit_value(bit_value),
      .send(send),
      .clk(clk),
      .rst(rst)
   );
   
   initial begin

      $dumpfile("vars.vcd");
      $dumpvars;

      clk = 0;
      forever begin
	 #1 clk = ~clk;
      end
   end

   initial begin

      reset();
      send_bit(0);
      send_bit(1);
      send_bit(0);
      send_bit(0);
      send_bit(1);
      send_bit(1);

      
      $display("SUCCESS");
      $finish;
      
   end

   // Stop simulation after a fixed time (timeout)
   initial begin
      #TIMEOUT $display("ERROR: timed out");
      $finish;
   end
   
endmodule
