set(CMAKE_HOST_SYSTEM "Linux-6.5.0-1023-oem")
set(CMAKE_HOST_SYSTEM_NAME "Linux")
set(CMAKE_HOST_SYSTEM_VERSION "6.5.0-1023-oem")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/home/john/Documents/git/axilite_neopixel/platform/microblaze_0/standalone_microblaze_0/bsp/microblaze_toolchain.cmake")

set(CMAKE_SYSTEM "Generic")
set(CMAKE_SYSTEM_NAME "Generic")
set(CMAKE_SYSTEM_VERSION "")
set(CMAKE_SYSTEM_PROCESSOR "microblaze")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
