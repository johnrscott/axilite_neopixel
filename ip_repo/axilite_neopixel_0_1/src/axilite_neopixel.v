`default_nettype none

`define OKAY 2'b00

//! @title AXI4-Lite Neopixel Driver
//!
//! 
module axilite_neopixel #(
   parameter CLOCK_FREQ = 100_000_000,
   parameter NUM_NEOPIXELS = 8
)(
   output wire	     dout, //! Neopixel data line output
   
   input wire	     arvalid, //! Read address channel VALID
   output wire	     arready, //! Read address channel READY
   input wire [31:0] araddr,  //! Read address channel ADDRESS
   input wire [2:0]  arprot,  //! Read address channel PROTECTION

   output reg	     rvalid,  //! Read data channel VALID
   input wire	     rready,  //! Read data channel READY
   output reg [31:0] rdata,   //! Read data channel DATA
   output wire [2:0] rresp,   //! Read data channel RESPONSE

   input wire	     awvalid, //! Write address channel VALID
   output wire	     awready, //! Write address channel READY
   input wire [31:0] awaddr,  //! Write address channel ADDRESS
   input wire [2:0]  awprot,  //! Write address channel PROTECTION

   input wire	     wvalid,  //! Write data channel VALID
   output reg	     wready,  //! Write data channel READY
   input wire [31:0] wdata,   //! Write data channel DATA
   
   output reg	     bvalid,  //! Write response channel VALID
   input wire	     bready,  //! Write response channel READY
   output wire [2:0]  bresp,   //! Write data channel DATA
   
   input wire	     aclk,    //! Global clock
   input wire	     aresetn  //! Global active-low reset
);

   wire	rst;
   assign rst = !aresetn;
   
   wire [11:0] read_addr, write_addr;
   assign read_addr = araddr[13:2];
   assign write_addr = awaddr[13:2];
   
   wire [23:0] read_data, write_data;
   assign write_data = wdata[23:0];
   
   buffer #(
      .CLOCK_FREQ(CLOCK_FREQ),
      .NUM_NEOPIXELS(NUM_NEOPIXELS)
   ) buffer_instance (
      .dout(dout),
      .read_data(read_data),
      .read_addr(read_addr),
      .read_en(rready),
      .write_data(write_data),
      .write_addr(write_addr),
      .write_en(wready),
      .rst(rst),
      .clk(aclk)
   );

   always @(posedge aclk) begin: write_data_channel_handshake
      if (rst)
	wready <= 0;
      else
	// Following ZipCPU, throttle write address/data
	// channels to account for backpressure from
	// controller bready delays. 
	wready <= !wready &&
		  (awvalid && wvalid) &&
		  (!bvalid || bready);
   end

   // write address channel handshake
   assign awready = wready;

   always @(posedge aclk) begin: write_response_channel_handshake
      if (rst)
	bvalid <= 0;
      else if (wready)
	bvalid <= 1;
      else if (bready)
	bvalid <= 0;
   end

   always @(posedge aclk) begin: read_data_channel_handshake
      if (rst)
	rvalid <= 0;
      else if (arvalid && arready)
	rvalid <= 1;
      else if (rready)
	rvalid <= 0;
   end

   // read address channel handshake
   assign arready = !rvalid; 
   
   always @(posedge aclk) begin: read_data_channel_response
      if (rst)
	rdata <= 0;
      else if (!rvalid || rready)
	rdata <= { 8'b0, read_data };
   end
   
   assign rresp = `OKAY;
   assign bresp = `OKAY;

   
endmodule
