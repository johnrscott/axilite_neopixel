`default_nettype none

module flush_datapath #(
   parameter CLOCK_FREQ = 100_000_000,
   parameter NUM_NEOPIXELS = 8,
   parameter NEOPIXEL_RESET_DELAY = 50e-6
)(
   output wire			     dout,
   output wire			     busy,
   output wire			     counter_gt_max,
   output wire			     counter_gt_buflen,
   input wire [24*NUM_NEOPIXELS-1:0] brg,
   input wire			     latch,
   input wire			     rst_counter,
   input wire			     inc_counter,
   input wire			     send,
   input wire			     rst,
   input wire			     clk
);

   // Set max counter ticks for Neopixel delay
   localparam [31:0] MAX = CLOCK_FREQ * NEOPIXEL_RESET_DELAY;
   
   // Copy of the buffer to ensure data sent to
   // Neopixels is in a consistent state.
   reg [24*NUM_NEOPIXELS-1:0] brg_copy;

   // Counter to point to current bit being sent
   // and then to time the 50 us delay.
   reg [31:0]		      counter;
   
   // Current bit being sent
   wire			      bit_value;

   // Extract the current bit to send from the
   // buffer. 
   assign bit_value = brg_copy[counter];

   // Set the counter size flags
   assign counter_gt_max = counter > MAX;
   assign counter_gt_buflen = counter > (24*NUM_NEOPIXELS-1);
   
   bit_sender #(
      .CLOCK_FREQ(CLOCK_FREQ)
   ) sender (
      .dout(dout),
      .busy(busy),
      .send(send),
      .bit_value(bit_value),
      .clk(clk),
      .rst(rst)
   );

   always @(posedge clk) begin: update_counter
      if (rst | rst_counter)
	counter <= 0;
      else if (inc_counter)
	counter <= counter + 1;
   end

   integer i;
   always @(posedge clk) begin: latch_brg
      if (rst)
	for (i = 0; i < 24*NUM_NEOPIXELS; i = i+1)
	  brg_copy[i] <= 0;
      if (latch)
	brg_copy <= brg;
   end

endmodule
