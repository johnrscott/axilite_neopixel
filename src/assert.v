`define assert(condition, fail_message) \
if (!(condition)) begin \
$display(fail_message); \
$finish; \
end
