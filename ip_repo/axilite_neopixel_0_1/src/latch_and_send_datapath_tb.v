`define assert(condition, fail_message) \
if (condition) begin \
$display(fail_message); \
$finish; \
end

module latch_and_send_datapath_tb();

   // Stop the simulation if it runs too long
   localparam TIMEOUT = 1500;

   localparam NUM_NEOPIXELS = 3;
   localparam CLOCK_FREQ = 32_000_000;
   localparam NEOPIXEL_PERIOD_COUNT = 40; // 1.25us
   
   reg clk, rst;

   reg send_bit, rst_neopixel_counter, inc_neopixel_counter,
       rst_bit_counter, inc_bit_counter, rst_delay_counter,
       inc_delay_counter, latch_rgb, load_current_brg, read_en,
       write_en;
       
   wire bit_counter_24, neopixel_counter_done, delay_done,
	busy, bit_busy, dout;

   reg [31:0] write_addr, read_addr, write_data;
   wire [31:0] read_data;
   
   task reset;
      begin
	 send_bit = 0;
	 rst_neopixel_counter = 0;
	 inc_neopixel_counter = 0;
	 rst_bit_counter = 0;
	 inc_bit_counter = 0;
	 rst_delay_counter = 0;
	 inc_delay_counter = 0;
	 latch_rgb = 0;
	 load_current_brg = 0;
	 
	 #4 rst = 1;
	 #4 rst = 0;
	 #4;
      end
   endtask
   
   initial begin

      $dumpfile("vars.vcd");
      $dumpvars;

      clk = 0;
      forever begin
	 #1 clk = ~clk;
      end
   end
   
   latch_and_send_datapath #(
      .CLOCK_FREQ(CLOCK_FREQ),
      .NUM_NEOPIXELS(NUM_NEOPIXELS)
   ) datapath (
      .busy(busy),
      .dout(dout),
      .send_bit(send_bit),
      .read_en(read_en),
      .read_data(read_data),
      .read_addr(read_addr),
      .write_en(write_en),
      .write_data(write_data),
      .write_addr(write_addr),
      .rst_neopixel_counter(rst_neopixel_counter),
      .inc_neopixel_counter(inc_neopixel_counter),
      .rst_bit_counter(rst_bit_counter),
      .inc_bit_counter(inc_bit_counter),
      .rst_delay_counter(rst_delay_counter),
      .inc_delay_counter(inc_delay_counter),
      .latch_rgb(latch_rgb),
      .bit_busy(bit_busy),
      .load_current_brg(load_current_brg),
      .bit_counter_24(bit_counter_24),
      .delay_done(delay_done),
      .neopixel_counter_done(neopixel_counter_done),
      .clk(clk),
      .rst(rst)
   );

   
   initial begin
      reset();
   end
   
   // Stop simulation after a fixed time (timeout)
   initial begin
      #TIMEOUT $display("ERROR: timed out");
      $finish;
   end

   
endmodule
