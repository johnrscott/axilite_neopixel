`default_nettype none

`include "assert.v"

`define CLEAN 1'b0
`define DIRTY 1'b1

`define IDLE 3'b000
`define LATCH 3'b001
`define SEND_BIT 3'b010
`define WAIT 3'b011
`define DELAY_50 3'b100

module flush_ctrl_tb();

   // Stop the simulation if it runs too long
   localparam TIMEOUT = 10000;
   
   reg	      clk, rst, write_en, busy, counter_gt_max, counter_gt_buflen;
   wire	      latch, inc_counter, rst_counter, send;
   
   flush_ctrl uut (
      .busy(busy),
      .write_en(write_en),
      .counter_gt_max(counter_gt_max),
      .counter_gt_buflen(counter_gt_buflen),
      .latch(latch),
      .rst_counter(rst_counter),
      .inc_counter(inc_counter),
      .send(send),
      .clk(clk),
      .rst(rst)
   );
   
   task reset;
      begin
	 write_en = 0;
	 busy = 0;
	 counter_gt_max = 0;
	 counter_gt_buflen = 0;
	 #4 rst = 1;
	 #4 rst = 0;
	 #4;
      end
   endtask

   task check_dirty_state;
     begin

	// Check the state is initially clean
	`assert(!uut.dirty, "ERROR: Expected initial state clean");
	
	write_en = 1;

	// Check that the state has changed to dirty
	#2 `assert(uut.dirty, "ERROR: Expected dirty after write_en");

	#2 reset();
	
     end
   endtask

   // The second write triggers another write_en in the middle
   // of the transaction, which should cause another buffer send
   // to start
   task check_send_sequence (input second_write);
     begin

	// Trigger a send
	write_en = 1;
	#2 write_en = 0;
	
	// Expect the correct sequence to set up the send
	#2; // Two cycles required, one for DIRTY state and another for LATCH
	`assert(latch, "ERROR: missing latch");
	`assert(rst_counter, "ERROR: missing rst_counter");

	// Expect the start of a bit send in the next cycle
	#2;
	`assert(send, "ERROR: missing send");
	`assert(inc_counter, "ERROR: missing inc_counter");

	// In the next cycles, the data path will be sending a
	// bit -- this simulates the datapath busy signal
	#2 busy = 1;

	// Assert a second write_en here. This should set the
	// state to DIRTY
	if (second_write) begin

	   `assert(uut.flush_state == `CLEAN, "ERROR: expected CLEAN state");
	   
	   write_en = 1;
	   #2 write_en = 0;

	   `assert(uut.flush_state == `DIRTY, "ERROR expected DIRTY state.")
	end
			 
	// Wait an indefinite number of cycles and expect the
	// WAITING state
	#20 `assert(uut.state == `WAIT, "ERROR: Expected waiting state");
	`assert(!send, "ERROR: invalid send");
	`assert(!inc_counter, "ERROR: invalid inc_counter");

	// Deassert busy to mark end of send, and expect the
	// send bit state
	busy = 0;
	#2;
	`assert(send, "ERROR: missing send");
	`assert(inc_counter, "ERROR: missing inc_counter");
	#2 busy = 1;
	
	// Next, also assert counter_gt_buflen to indicate
	// end of buffer send
	#20;
	busy = 0;
	counter_gt_buflen = 1;
	#1 `assert(rst_counter, "ERROR: expected rst_counter");
	#1 `assert(uut.state == `DELAY_50, "ERROR: expected DELAY_50 state");

	// Finally, check that the DELAY_50 state remains until
	// counter_gt_max is asserted
	#50 `assert(uut.state == `DELAY_50, "ERROR: expected DELAY_50 state");
	counter_gt_max = 1;
	#1 `assert(uut.done, "ERROR: expected done");
	#1;
	
	// Expect return to idle state
	`assert(uut.state == `IDLE, "ERROR: expected IDLE state");

	// If the second write was performed, expect another buffer send
	if (second_write) begin
	  #2 `assert(uut.state == `LATCH, "ERROR: expected second write to start");
	end else begin
	  #2 `assert(uut.state == `IDLE, "ERROR: expected remain in IDLE state");
	end
	
     end
   endtask

      
   initial begin

      $dumpfile("vars.vcd");
      $dumpvars;

      clk = 0;
      forever begin
	 #1 clk = ~clk;
      end
   end


   initial begin

      reset();
      check_send_sequence(0);

      reset();
      check_send_sequence(1);
      
      $display("SUCCESS");
      $finish;
      
   end
   
   // Stop simulation after a fixed time (timeout)
   initial begin
      #TIMEOUT $display("ERROR: timed out");
      $finish;
   end
   

endmodule
