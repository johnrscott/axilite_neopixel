`default_nettype none

`define IDLE 2'b00
`define HIGH_ZERO 2'b01
`define HIGH_ONE 2'b10
`define LOW 2'b11

//! @title Send a single bit to the Neopixel `dout` data line
//!
//! This module sends a single bit to the Neopixel data line. The 
//! data format is PWM (high then low), with logical one/zero determined
//! by the duty cycle (the high-time). Each cycle of PWM sends one bit.
//!
module bit_sender #(
    parameter CLOCK_FREQ = 100_000_000, //! Frequency of the `clk` input.
    parameter NEOPIXEL_PERIOD = 1.25e-6, //! Neopixel bit-period (800kHz).
    parameter ZERO_HIGH_TIME = 0.3e-6, //! High-time when sending logical 0.
    parameter ONE_HIGH_TIME = 0.6e-6 //! High-time when sending logical 1.
  )(
    output reg dout, //! Neopixel data line.
    output reg busy, //! Indicates that a bit is currently being sent.
    input wire bit_value, //! Bit value to be sent to Neopixel on `send`.
    input wire send, //! Triggers sending a bit if `!busy`.
    input wire clk, //! Clock used to define high/low bit periods
    input wire rst //! Synchronous active-high reset
  );

  localparam [31:0] T = NEOPIXEL_PERIOD * CLOCK_FREQ;
  localparam [31:0] L = ZERO_HIGH_TIME * CLOCK_FREQ;
  localparam [31:0] H = ONE_HIGH_TIME * CLOCK_FREQ;

  reg [1:0]	  state, next_state;

  //! The counter begins when the sending starts (`dout` is asserted). After
  //! it reaches either T or L (depending on whether 1 or 0 is sent), 
  //! `dout` is set to 0 for the remainder of this PWM cycle. 
  reg [31:0]	  count;

  // If one, increment counter. If zero,m
  // reset counter back to zero
   reg		  inc_count;
   
  always @(posedge clk)
  begin: update_state
    if (rst)
      state <= `IDLE;
    else
      state <= next_state;
  end

   always @(*)
     begin: derive_moore_outputs
	inc_count = 0;
	dout = 0;
	busy = 0;
	case (state)
	  `IDLE:;
	  `HIGH_ZERO, `HIGH_ONE:
	    begin
               inc_count = 1;
               dout = 1;
               busy = 1;
	    end
	  `LOW:
	    begin
               inc_count = 1;
               busy = 1;
	    end
	endcase
     end

  always @(*)
  begin: calculate_next_state
    next_state = state;
    case (state)
      `IDLE:
        if (send)
          next_state = bit_value ? `HIGH_ONE : `HIGH_ZERO;
      `HIGH_ZERO:
          if (count > L)
            next_state = `LOW;
      `HIGH_ONE:
          if (count > H)
            next_state = `LOW;
      `LOW:
        if (count > T)
          next_state = `IDLE;
    endcase
  end // always @ (*)

  always @(posedge clk)
  begin: counter_data_path
    if (rst)
      count <= 0;
    else if (inc_count)
      count <= count + 1;
    else
      count <= 0;
  end

endmodule
