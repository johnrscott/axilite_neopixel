`default_nettype none

`include "assert.v"

module flush_datapath_tb();

   // Stop the simulation if it runs too long
   localparam TIMEOUT = 10000;
   
   // Set the clock frequency and calculate neopixel
   // high times for 0 and 1. These are used to make
   // approximate checks of the timing.
   localparam CLOCK_FREQ = 32_000_000;
   localparam NEOPIXEL_PERIOD_COUNT = 40; // 1.25us
   localparam ZERO_HIGH_COUNT = 10; // 0.3us
   localparam ONE_HIGH_COUNT = 20; // 0.6us

   localparam NUM_NEOPIXELS = 3;
   localparam NEOPIXEL_RESET_DELAY = 0.5e-6;
   
   reg	      clk, rst, latch, inc_counter, rst_counter, send;
   wire	      dout, busy, counter_gt_max;

   reg [24*NUM_NEOPIXELS-1:0] brg;
   
   flush_datapath #(
      .CLOCK_FREQ(CLOCK_FREQ),
      .NUM_NEOPIXELS(NUM_NEOPIXELS),
      .NEOPIXEL_RESET_DELAY(NEOPIXEL_RESET_DELAY)
   ) uut (
      .dout(dout),
      .busy(busy),
      .counter_gt_max(counter_gt_max),
      .brg(brg),
      .latch(latch),
      .rst_counter(rst_counter),
      .inc_counter(inc_counter),
      .send(send),
      .clk(clk),
      .rst(rst)
   );
   
   task reset;
      begin
	 latch = 0;
	 inc_counter = 0;
	 rst_counter = 0;
	 send = 0;
	 #4 rst = 1;
	 #4 rst = 0;
	 #4;
      end
   endtask

   // Check that the counter reset/increment works, and
   // that the flag for counter > MAX gives the correct
   // result for the 0.5 us delay above.
   task test_counter;
      begin

	 // Reset the counter
	 rst_counter = 1;
	 #2 rst_counter = 0;

	 // Check the counter is equal to zero
	 `assert(uut.counter == 0, "ERROR: Failed to reset counter");

	 // Start incrementing counter
	 inc_counter = 1;
	 #20 inc_counter = 0;

	 // Clock period is #2, so expect counter == 10
	 `assert(uut.counter == 10, "ERROR: Failed to increment counter");
	 
	 #20;

	 // Expect counter is constant
	 `assert(uut.counter == 10, "ERROR: Expected constant counter");

	 // Test another increment
	 inc_counter = 1;
	 #2 inc_counter = 0;

	 // Expect counter is constant
	 `assert(uut.counter == 11, "ERROR: Failed second counter increment");

	 // Test reset after increment
	 rst_counter = 1;
	 #2 rst_counter = 0;

	 // Expect counter is constant
	 `assert(uut.counter == 0, "ERROR: Failed second counter reset");

	 // Test counter > max signal. (MAX = )
	 `assert(counter_gt_max == 0, "ERROR: Expected counter <= MAX");

	 // Begin incrementing
	 inc_counter = 1;
	 
	 // Expect > MAX here
	 #32;
	 `assert(counter_gt_max == 0, "ERROR: Still expected counter <= MAX");
	 #2;
	 `assert(counter_gt_max == 1, "ERROR: Expected counter > MAX");
	 
      end
   endtask

   integer i;
   reg	   equal;

   // Check that the latch control signal copies data from
   // brg to brg_copy
   task test_latch;
     begin
	for (i = 0; i < 24*NUM_NEOPIXELS; i = i+1)
	  brg[i] = i;

	// Expect inequality between brg and brg_copy
	equal = 1;
	for (i = 0; i < 24*NUM_NEOPIXELS; i = i+1)
	  if (uut.brg_copy[i] != brg[i])
	    equal = 0;
	`assert(equal == 0, "ERROR: expected brg != brg_copy");
	
	latch = 1;
	#2 latch = 0;

	// Expect equality between brg and brg_copy
	equal = 1;
	for (i = 0; i < 24*NUM_NEOPIXELS; i = i+1)
	  if (uut.brg_copy[i] != brg[i])
	    equal = 0;
	`assert(equal == 1, "ERROR: latch did not load brg_copy correctly");
	
     end
   endtask

   task test_bit_send;
      begin

	 // Load alternating pattern of ones and zeros
	 for (i = 0; i < 24*NUM_NEOPIXELS; i=i+1)
	   brg[i] = i;
	 
	 // Latch the buffer
	 latch = 1;
	 #2 latch = 0;

	 // Reset the counter
	 rst_counter = 1;
	 #2 rst_counter = 0;

	 // Trigger a send
	 send = 1;
	 
	 // Send each bit
	 for (i = 0; i < 24*NUM_NEOPIXELS; i=i+1) begin
	    
	    // Increment the counter for next send -- this
	    // prepares the next bit for sending
	    inc_counter = 1;
	    #2 inc_counter = 0;

	    // Wait for half the PWM cycle and sample dout
	    #NEOPIXEL_PERIOD_COUNT;
	    `assert(dout == brg[i], "ERROR: Incorrect bit value transmitted");
	    
	    // Await the end of the bit transmission
	    while (busy)
	      #1;
	 end
	 
      end
   endtask
   
   initial begin

      $dumpfile("vars.vcd");
      $dumpvars;

      clk = 0;
      forever begin
	 #1 clk = ~clk;
      end
   end


   initial begin

      reset();

      test_counter();
      test_latch();
      test_bit_send();
      
      $display("SUCCESS");
      $finish;
      
   end
   
   // Stop simulation after a fixed time (timeout)
   initial begin
      #TIMEOUT $display("ERROR: timed out");
      $finish;
   end
   

endmodule
