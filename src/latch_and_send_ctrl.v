`default_nettype none

`define IDLE 3'b000
`define LATCH 3'b001
`define LOAD_BRG 3'b010
`define SEND_BIT 3'b011
`define WAITING 3'b100
`define DELAY_50 3'b101

//! @title Control state machine for `latch_and_send` module
//!
//!
module latch_and_send_ctrl (
   output reg busy,		     //! Asserted while the buffer is being sent to the Neopixels.
   output reg send_bit,		     //! Trigger sending a single bit (`send` input to `bit_sender`).
   output reg rst_neopixel_counter,
   output reg inc_neopixel_counter,
   output reg rst_bit_counter,
   output reg inc_bit_counter,
   output reg rst_delay_counter,
   output reg inc_delay_counter,
   output reg latch_rgb,	     //! Trigger a copy of the RGB buffer before sending.
   output reg load_current_brg,	     //! Load a 24-bit RGB value into the BRG buffer.
   input wire send,		     //! Assert to trigger a full-RGB-buffer send.
   input wire bit_busy,		     //! Asserted by `bit_sender` module during send PWM cycle.
   input wire delay_done,	     //! Set when the delay counter  
   input wire bit_counter_24,	     //! Set when bit counter is 24
   input wire neopixel_counter_done, //! Set when the neopixel counter is NUM_NEOPIXELS

   input wire clk,
   input wire rst
);

   reg [2:0] state, next_state;
   
   always @(posedge clk) begin: update_state
      if (rst)
	state <= `IDLE;
      else
	state <= next_state;
   end

   always @(*) begin: calculate_control_signals

      busy = 1;
      send_bit = 0;
      rst_neopixel_counter = 0;
      inc_neopixel_counter = 0;
      rst_bit_counter = 0;
      inc_bit_counter = 0;
      rst_delay_counter = 0;
      inc_delay_counter = 0;
      latch_rgb = 0;
      load_current_brg = 0;

      case (state)
	`IDLE: busy = 0;
	`LATCH: begin
	   latch_rgb = 1;
	   rst_neopixel_counter = 1;
	   rst_bit_counter = 1;
	   rst_delay_counter = 1;
	end
	`LOAD_BRG: begin
	   load_current_brg = 1;
	   inc_neopixel_counter = 1;
	end
	`SEND_BIT: send_bit = 1;
	`WAITING: if (!bit_busy && !bit_counter_24)
	  inc_bit_counter = 1;
	`DELAY_50: inc_delay_counter = 1;
      endcase
   end
   
   always @(*) begin: calculate_next_state
      next_state = state;
      case (state)
	`IDLE: if (send)
	  next_state = `LATCH;
	`LATCH: next_state = `LOAD_BRG;
	`LOAD_BRG: next_state = `SEND_BIT;
	`SEND_BIT: if (bit_busy)
	  next_state = `WAITING;
	`WAITING: begin
	   if (!bit_busy && bit_counter_24) begin
	      if (neopixel_counter_done)
		next_state = `DELAY_50;
	      else
		next_state = `LOAD_BRG;
	   end
	end
	`DELAY_50: if (delay_done)
	  next_state = `IDLE;
      endcase
   end

   
endmodule
