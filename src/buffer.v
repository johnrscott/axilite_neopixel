`default_nettype none

//! @title Neopixel RGB buffer read/write interface
//!
//! This is the main interface to the RGB buffer for the
//! Neopixels. It is word-readable and word-writable. The
//! buffer is addressed using one word per Neopixel (the
//! first three bytes contain the RGB data, and the last
//! byte will always return 0 -- writes to the final byte
//! are ignored).
//!
//! Writes are synchronous with `clk`, and always succeed
//! in one clock cycle. Reads are asynchronous.
module buffer #(
   parameter CLOCK_FREQ = 100_000_000,
   parameter NUM_NEOPIXELS = 8
) (
   output wire	      dout,	  //! Data output to Neopixel
   output wire [23:0] read_data,  //! RGB buffer read data
   input wire [11:0]  read_addr,  //! Neopixel read-index into the BRG buffer
   input wire	      read_en,	  //! Assert to read RGB buffer
   input wire [23:0]  write_data, //! Data to write into BRG buffer
   input wire [11:0]  write_addr, //! Neopixel read-index into the BRG buffer
   input wire	      write_en,	  //! Assert to write to RGB buffer
   input wire	      rst,
   input wire	      clk
);

   reg [23:0]		      brg [0:NUM_NEOPIXELS-1];

   // Pack the buffer into a flat bit-array to pass
   // as a Verilog port.
   wire [24*NUM_NEOPIXELS-1:0] packed_brg;
   genvar		       n;
   generate
      for (n = 0; n < NUM_NEOPIXELS; n = n + 1)
	assign packed_brg[24*n +: 24] = brg[n];
   endgenerate
   
   
   // Trigger a flush to the Neopixels any time
   // a write occurs, including if a write occurs
   // while a previous flush (send) is in progress.
   flush #(
      .CLOCK_FREQ(CLOCK_FREQ),
      .NUM_NEOPIXELS(NUM_NEOPIXELS)
   ) flush_instance (
      .dout(dout),
      .write_en(write_en),
      .brg(packed_brg),
      .clk(clk),
      .rst(rst)
   );

   assign read_data = { 8'b0, brg[read_addr] };

   integer i;
   always @(posedge clk) begin: write_to_brg
      if (rst)
	for (i = 0; i < 24*NUM_NEOPIXELS; i = i+1)
	  brg[i] <= 0;
      else if (write_en)
	brg[write_addr] <= write_data;
   end
      
endmodule
