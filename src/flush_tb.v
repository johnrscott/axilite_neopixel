`default_nettype none

`include "assert.v"

`define CLEAN 1'b0
`define DIRTY 1'b1

`define IDLE 3'b000
`define LATCH 3'b001
`define SEND_BIT 3'b010
`define WAIT 3'b011
`define DELAY_50 3'b100

module flush_tb();

   // Stop the simulation if it runs too long
   localparam TIMEOUT = 50000;
   
   // Set the clock frequency and calculate neopixel
   // high times for 0 and 1. These are used to make
   // approximate checks of the timing.
   localparam CLOCK_FREQ = 32_000_000;
   localparam NUM_NEOPIXELS = 2;
   localparam NEOPIXEL_RESET_DELAY = 50e-6;
   
   reg	      clk, rst, write_en;
   wire	      dout;

   reg [24*NUM_NEOPIXELS-1:0] brg;
   
   flush #(
      .CLOCK_FREQ(CLOCK_FREQ),
      .NUM_NEOPIXELS(NUM_NEOPIXELS),
      .NEOPIXEL_RESET_DELAY(NEOPIXEL_RESET_DELAY)
   ) uut (
      .dout(dout),
      .brg(brg),
      .write_en(write_en),
      .clk(clk),
      .rst(rst)
   );
   
   task reset;
      begin
	 write_en = 0;
	 #4 rst = 1;
	 #4 rst = 0;
	 #4;
      end
   endtask
   
   initial begin

      $dumpfile("vars.vcd");
      $dumpvars;

      clk = 0;
      forever begin
	 #1 clk = ~clk;
      end
   end

   integer i;
   task load_test_pattern_1;
     begin
	for (i = 0; i < 24*NUM_NEOPIXELS; i=i+1)
	  brg[i] = i[0];
     end
   endtask
      
   initial begin

      reset();
      load_test_pattern_1();
      
      // Send the data
      write_en = 1;
      #2 write_en = 0;

      // Wait for transmission to start
      #4;
      
      // Wait for end of send
      while (uut.flush_ctrl_instance.state != `IDLE)
	#1;
		    

      // Wait a defined period, then trigger a send
      // and perform a write directly after to prove
      // the flush works
      #200;

      // Send the data the second time
      write_en = 1;
      #2 write_en = 0;

      // Wait for transmission to start
      #4;

      // Send the data a third time -- should
      // trigger another send after the second.
      write_en = 1;
      #2 write_en = 0;
      
      // Wait for end of second send
      while (uut.flush_ctrl_instance.state != `IDLE)
	#1;

      // Wait for transmission to start
      #4;
      
      // Wait for end of second send
      while (uut.flush_ctrl_instance.state != `IDLE)
	#1;
      
      $display("SUCCESS");
      $finish;
      
   end
   
   // Stop simulation after a fixed time (timeout)
   initial begin
      #TIMEOUT $display("ERROR: timed out");
      $finish;
   end
   

endmodule
