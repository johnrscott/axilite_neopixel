`default_nettype none

`include "assert.v"

`define CLEAN 1'b0
`define DIRTY 1'b1

`define IDLE 3'b000
`define LATCH 3'b001
`define SEND_BIT 3'b010
`define WAIT 3'b011
`define DELAY_50 3'b100

module axilite_neopixel_tb();

   // Stop the simulation if it runs too long
   localparam TIMEOUT = 50000;
   
   // Set the clock frequency and calculate neopixel
   // high times for 0 and 1. These are used to make
   // approximate checks of the timing.
   localparam CLOCK_FREQ = 32_000_000;
   localparam NUM_NEOPIXELS = 2;

   // Global AXI4-Lite signals
   reg	      aclk, aresetn;

   // Read address channel
   reg	      arvalid;
   reg [31:0] araddr;
   wire	      arready;

   // Read data channel
   reg	      rready;
   wire	      rvalid;
   wire [31:0] rdata; 

   // Write address channel
   reg	       awvalid;
   reg [31:0]  awaddr;
   wire	      awready;

   // Write data channel
   reg	      wvalid;
   reg [31:0] wdata;
   wire	      wready;

   // Write response channel
   reg	      bready;
   wire	      bvalid;
   
   // Data out to Neopixel
   wire	      dout;
   
   axilite_neopixel #(
      .CLOCK_FREQ(CLOCK_FREQ),
      .NUM_NEOPIXELS(NUM_NEOPIXELS)
   ) uut (
      .dout(dout),

      .arvalid(arvalid),
      .araddr(araddr),
      .arready(arready),
      
      .rready(rready),
      .rvalid(rvalid),
      .rdata(rdata),

      .wdata(wdata),
      .wvalid(wvalid),
      .wready(wready),
      
      .awvalid(awvalid),
      .awaddr(awaddr),
      .awready(awready),

      .bready(bready),
      .bvalid(bvalid),
      
      .aclk(aclk),
      .aresetn(aresetn)
   );
   
   task reset;
      begin

	 araddr = 0;
	 arvalid = 0;

	 rready = 0;
	 
	 awaddr = 0;
	 awvalid = 0;

	 wdata = 0;
	 wvalid = 0;

	 bready = 0;
	 
	 #4 aresetn = 0;
	 #4 aresetn = 1;
	 #4;
      end
   endtask

   task simple_write(input [31:0] data, input [31:0] addr);
      begin

	 awaddr = addr;
	 awvalid = 1;
	 
	 wdata = data;
	 wvalid = 1;

	 // Wait for write address/data ready
	 while (awvalid || wvalid) begin
	    #1;

	    if (awready) awvalid = 0;
	    if (wready) wvalid = 0;

	 end

	 // Wait for write response
	 while (!bvalid)
	   #1;

	 // Set bready and await handshake
 	 bready = 1;
	 while (bvalid)
	   #1;
 	 bready = 0;	     
	 
      end
   endtask

   task simple_read;
      begin

	 araddr = 0;
	 arvalid = 1;

	 #4 arvalid = 0;
	 
	 #4 rready = 1;
	 #2 rready = 0;
		     
	    
      end
   endtask

   
   wire [2:0] state;
   assign state = uut.buffer_instance.flush_instance.flush_ctrl_instance.state;
   task await_dout_end;
      begin

      // Wait for transmission to start
      while(state == `IDLE)
	#1;

      // Wait for transmission to end
      while(state != `IDLE)
	#1;
	 
      end
   endtask
   
   initial begin

      $dumpfile("vars.vcd");
      $dumpvars;

      aclk = 0;
      forever begin
	 #1 aclk = ~aclk;
      end
   end
      
   initial begin

      reset();
      simple_write(32'habcd, 0);
      await_dout_end();
      simple_read();

      // Do two writes and expect two transmissions
      simple_write(32'h0, 0);
      simple_write(32'hffff_ff, 0);
      await_dout_end();
      await_dout_end();

      // Do two more writes to the other Neopixel
      simple_write(32'h0, 4);
      simple_write(32'hffff_ff, 4);
      await_dout_end();
      await_dout_end();

      
      $display("SUCCESS");
      $finish;
      
   end
   
   // Stop simulation after a fixed time (timeout)
   initial begin
      #TIMEOUT $display("ERROR: timed out");
      $finish;
   end
   

endmodule
