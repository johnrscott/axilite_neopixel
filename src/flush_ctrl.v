`default_nettype none

`define CLEAN 1'b0
`define DIRTY 1'b1

`define IDLE 3'b000
`define LATCH 3'b001
`define SEND_BIT 3'b010
`define WAIT 3'b011
`define DELAY_50 3'b100

//! @title Controller for `flush` module
//!
module flush_ctrl(
    output reg send,	 //! Trigger sending the latched buffer
    output reg latch,
    output reg rst_counter,
    output reg inc_counter,
    input wire busy,	 //! Set while the datapath is sending the latched buffer
    input wire write_en, // Write-enable signal from rgb, used to trigger send
    input wire counter_gt_max,
    input wire counter_gt_buflen,
    input wire clk,
    input wire rst
);
   
   // State machine 1: clean/dirt state for flush
   // Only output is flush_state which is used by
   // state machine 2.
   
   reg flush_state, next_flush_state;

   reg done;
   wire	dirty;

   assign dirty = flush_state == `DIRTY;
   
   always @(posedge clk)
     begin: update_flush_state
	if (rst)
	  flush_state <= `CLEAN;
	else
	  flush_state <= next_flush_state;
     end
   
   always @(*) begin: calculate_next_flush_state
      next_flush_state = flush_state;
      case (flush_state)
	`CLEAN: if (write_en)
	  next_flush_state = `DIRTY;
	`DIRTY: if (latch & !write_en)
	  next_flush_state = `CLEAN;
      endcase
   end
      
   // State machine 2: Send bits while in flush state
   
   reg [2:0] state, next_state;

   always @(posedge clk)
     begin: update_state
	if (rst)
	  state <= `IDLE;
	else
	  state <= next_state;
     end
   
   always @(*)
     begin: calculate_outputs
	send = 0;
	latch = 0;
	rst_counter = 0;
	inc_counter = 0;
	done = 0;
	case (state)
	  `IDLE:;
	  `LATCH: begin
	     latch = 1;
	     rst_counter = 1;
	  end
	  `SEND_BIT: begin
	     send = 1;
	     inc_counter = 1;
	  end
	  `WAIT: if (!busy & counter_gt_buflen)
	    rst_counter = 1;
	  `DELAY_50: begin
	     inc_counter = 1;
	     if (counter_gt_max)
	       done = 1;
	  end
	endcase
     end

   always @(*)
     begin: calculate_next_state
	next_state = state;
	case (state)
	  `IDLE: if (dirty)
              next_state = `LATCH;
	  `LATCH: next_state = `SEND_BIT;
	  `SEND_BIT: next_state = `WAIT;
	  `WAIT: if (!busy)
	    if (counter_gt_buflen)
	      next_state = `DELAY_50;
	    else
	      next_state = `SEND_BIT;	  
	  `DELAY_50: if (counter_gt_max)
	    next_state = `IDLE;
	endcase
     end

endmodule
