`default_nettype none

//! @title Send the full RGB buffer to the Neopixels
//!
//! When idle, on `send`, this module latches a copy of the
//! `rgb` buffer input into an internal register and sends
//! it to the Neopixels bit-by-bit.
//!
//! The buffer `rgb` contains three bytes of data per Neopixel:
//! green (offset 0 bytes), red (offset 1) and blue (offset 2).
//! Data is sent out from low-to-high addresses (i.e. the order
//! green, red, blue), and each byte is sent most-significant-bit
//! first.
//!
//! Neopixels are wired together in a string, with each Neopixel
//! `din` connected to the `dout` of the previous Neopixel. The
//! signal `dout` from this module is connected to the `din` of
//! the first Neopixel.
//!
//! Each Neopixel accepts a stream of 24-bit GRB codes, takes the
//! first one received to set its own state, and passes the others
//! on to the next Neopixels in the chain.
//!
//! As a result, numbering Neopixels starting from 0 near this
//! module, Neopixel N receives data from rgb[N].
//!
//! The Neopixels only update their state after the data transmission]
//! has finished. This is indicated by a 50 us or longer period
//! of `!dout`. The `busy` signal continues to be asserted during this
//! delay.
//!
module latch_and_send #(
   parameter CLOCK_FREQ = 100_000_000,
   parameter NEOPIXEL_ADDR_WIDTH = 3,
   parameter NUM_NEOPIXELS = 8,
   parameter NEOPIXEL_RESET_DELAY = 50e-6
) (
   output wire	      dout,
   output wire	      busy,
   output wire [31:0] read_data,  //! RGB buffer read data
   input wire	      read_en,	  //! Assert to read RGB buffer
   input wire [31:0]  read_addr,  //! Byte-address in RGB buffer
   input wire	      write_en,	  //! Assert to write to RGB buffer
   input wire [31:0]  write_addr, //! Byte-address in RGB buffer
   input wire [31:0]  write_data, //! Data to write into RGB buffer
   input wire	      send,
   input wire	      rst,
   input wire	      clk
);

   wire send_bit, rst_neopixel_counter, inc_neopixel_counter,
	rst_bit_counter, inc_bit_counter, rst_delay_counter,
	inc_delay_counter, latch_rgb, load_current_brg, bit_busy,
	bit_counter_24, neopixel_counter_done, delay_done;
   
   latch_and_send_ctrl ctrl (
      .busy(busy),
      .send_bit(send_bit),
      .rst_neopixel_counter(rst_neopixel_counter),
      .inc_neopixel_counter(inc_neopixel_counter),
      .rst_bit_counter(rst_bit_counter),
      .inc_bit_counter(inc_bit_counter),
      .rst_delay_counter(rst_delay_counter),
      .inc_delay_counter(inc_delay_counter),
      .latch_rgb(latch_rgb),
      .load_current_brg(load_current_brg),
      .send(send),
      .bit_busy(bit_busy),
      .bit_counter_24(bit_counter_24),
      .delay_done(delay_done),
      .neopixel_counter_done(neopixel_counter_done),
      .clk(clk),
      .rst(rst)
   );
   
   latch_and_send_datapath #(
      .CLOCK_FREQ(CLOCK_FREQ),
      .NUM_NEOPIXELS(NUM_NEOPIXELS)
   ) datapath (
      .busy(busy),
      .dout(dout),
      .send_bit(send_bit),
      .read_en(read_en),
      .read_data(read_data),
      .read_addr(read_addr),
      .write_en(write_en),
      .write_data(write_data),
      .write_addr(write_addr),
      .rst_neopixel_counter(rst_neopixel_counter),
      .inc_neopixel_counter(inc_neopixel_counter),
      .rst_bit_counter(rst_bit_counter),
      .inc_bit_counter(inc_bit_counter),
      .rst_delay_counter(rst_delay_counter),
      .inc_delay_counter(inc_delay_counter),
      .latch_rgb(latch_rgb),
      .load_current_brg(load_current_brg),
      .bit_busy(bit_busy),
      .bit_counter_24(bit_counter_24),
      .delay_done(delay_done),
      .neopixel_counter_done(neopixel_counter_done),
      .clk(clk),
      .rst(rst)
   );
         
endmodule
