# 2024-06-25T21:32:20.346854
import vitis

client = vitis.create_client()
client.set_workspace(path="/home/john/Documents/git/axilite_neopixel")

platform = client.get_platform_component(name="platform")
status = platform.build()

status = platform.update_hw(hw = "/home/john/Documents/git/axilite_neopixel/neopixel_test/top_wrapper.xsa")

vitis.dispose()

