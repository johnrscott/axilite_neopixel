`default_nettype none

module latch_and_send_datapath #(
    parameter CLOCK_FREQ = 100_000_000,
    parameter NEOPIXEL_ADDR_WIDTH = 3, 
    parameter NEOPIXEL_RESET_DELAY = 50e-6,
    parameter NUM_NEOPIXELS = 8
)(
   output reg	      busy,
   output wire	      dout,
   output wire	      delay_done,
   output wire	      bit_counter_24,
   output wire	      neopixel_counter_done,
   output wire	      bit_busy,
   output wire [31:0] read_data,  //! RGB buffer read data
   input wire	      read_en,	  //! Assert to read RGB buffer
   input wire [31:0]  read_addr,  //! Byte-address in RGB buffer
   input wire	      write_en,	  //! Assert to write to RGB buffer
   input wire [31:0]  write_addr, //! Byte-address in RGB buffer
   input wire [31:0]  write_data, //! Data to write into RGB buffer
   input wire	      send_bit,
   input wire	      rst_neopixel_counter,
   input wire	      inc_neopixel_counter,
   input wire	      rst_bit_counter,
   input wire	      inc_bit_counter,
   input wire	      rst_delay_counter,
   input wire	      inc_delay_counter,
   input wire	      latch_rgb,
   input wire	      load_current_brg,
   input wire	      clk,
   input wire	      rst
);

   reg [23:0] rgb[NEOPIXEL_ADDR_WIDTH-1:0];
   
   localparam [31:0] T = NEOPIXEL_RESET_DELAY * CLOCK_FREQ;
   assign delay_done = (delay_counter > T);
   
   reg [23:0] latched_rgb [NEOPIXEL_ADDR_WIDTH-1:0];
   
   //! Points successively to each RGB word in the rgb
   //! buffer that must be sent (lower addresses are
   //! sent first)
   reg [NEOPIXEL_ADDR_WIDTH-1:0] neopixel_counter;
   assign neopixel_counter_done = (neopixel_counter == NUM_NEOPIXELS);
   
   //! The current BRG 24-bit value being sent. Note
   //! That the GRB send order is reversed so that bits
   //! can be sent least-significant-bit first from this
   //! buffer.
   reg [23:0]			 current_brg;
   
   //! Points successively to the bits in the current 
   //! BRG word being sent (bits are sent least-significant-
   //! bit first).
   reg [4:0]			 bit_counter;
   assign bit_counter_24 = (bit_counter == 24);

   reg [31:0]			 delay_counter;

   //! The bit currently being sent, which is taken from
   //! the current BRG register (bits reordered so that
   //! they can be transmitted from least- to most-significant)
   wire				 bit_value;

   
   assign bit_value = current_brg[bit_counter];
   
   bit_sender #(
      .CLOCK_FREQ(CLOCK_FREQ)
   ) bit_sender_0 (
      .dout(dout),
      .busy(bit_busy),
      .send(send_bit),
      .bit_value(bit_value),
      .clk(clk),
      .rst(rst)
   );

   
   always @(posedge clk) begin: update_neopixel_counter
      if (rst_neopixel_counter)
	neopixel_counter <= 0;
      else if (inc_neopixel_counter)
	neopixel_counter += 1;
   end

   always @(posedge clk) begin: update_bit_counter
      if (rst_bit_counter)
	bit_counter <= 0;
      else if (inc_bit_counter)
	bit_counter += 1;
   end

   always @(posedge clk) begin: update_delay_counter
      if (rst_delay_counter)
	delay_counter <= 0;
      else if (inc_delay_counter)
	delay_counter += 1;
   end

   integer i;
   always @(posedge clk) begin: update_latch_rgb
      if (latch_rgb)
	for (i = 0; i < NUM_NEOPIXELS; ++i) 
	  latched_rgb[i] <= rgb[i];
   end

   always @(posedge clk) begin: update_current_brg
      if (load_current_brg)
	current_brg <= { rgb[neopixel_counter][16+:8],
			 rgb[neopixel_counter][0+:8],
			 rgb[neopixel_counter][8+:8] };
   end

   // Logic to read/write the rgb buffer below
   
   assign read_data = { 8'b0, rgb[read_addr] };

   always @(posedge clk) begin: write_rgb_buffer
      if (rst)
	for (i = 0; i < NUM_NEOPIXELS; ++i)
	  rgb[i] <= 24'b0;
      else if (write_en)
	rgb[write_addr] <= write_data[23:0];
   end

   
endmodule
