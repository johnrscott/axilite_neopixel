#!/usr/bin/env bash

# Using Vivado 2023.2, after opening Vitis from Tools -> Launch
# Vitis IDE, when you attempt to create a platform component, you many
# get an error relating to lopper at the OS and processor step.
#
# This page has information about the error:
#
# https://support.xilinx.com/s/question/0D54U00007raz9SSAQ/
# vitis-unified-invalid-lopper-directory-error-on-linux?language=en_US
#
# Run this script (after running `source settings.sh`) to fix the lopper
# install for Vitis 2023.2

${XILINX_VITIS}/tps/lnx64/lopper-1.1.0-packages/install.sh \
    ${XILINX_VITIS}/tps/lnx64/python-3.8.3 \
    ${XILINX_VITIS}/lib/lnx64.o/Ubuntu/20 \
    ${XILINX_VITIS}/tps/lnx64/lopper-1.1.0-packages/py38/wheels \
    ${XILINX_VITIS}/tps/lnx64/lopper-1.1.0-packages/py38/requirements.txt \
    ${XILINX_VITIS}/tps/lnx64/lopper-1.1.0
