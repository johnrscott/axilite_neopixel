`default_nettype none

`define CLEAN 2'b00
`define DIRTY 2'b01
`define SEND 2'b10
`define DELAY_50 2'b11

//! @title Flush the BRG buffer to the Neopixels.
//!
//! The main function of this unit is triggering a send
//! when the `brg` buffer is written-to, and monitoring whether
//! any writes occur during the sending (which must trigger another
//! send).
//!
//! The ``CLEAN` state is when the Neopixels agree with the `rgb`
//! buffer. The ``DIRTY` state is used to queue a Neopixel refresh.
//!
//! The `send` output is used to interface to a module that latches
//! a copy of the `rgb` buffer and sends it to the Neopixels. This
//! module returns as signal `busy` which is asserted until the send
//! is complete.
module flush #(
   parameter CLOCK_FREQ = 100_000_000,
   parameter NUM_NEOPIXELS = 8,
   parameter NEOPIXEL_RESET_DELAY = 50e-6
)(
   output wire			     dout,
   input wire [24*NUM_NEOPIXELS-1:0] brg,
   input wire			     write_en,
   input wire			     clk,
   input wire			     rst
);

   wire	      inc_counter, rst_counter, send, latch,
	      busy, counter_gt_max, counter_gt_buflen;
   
   // flush_control
   flush_ctrl flush_ctrl_instance (
       .send(send),
       .latch(latch),
       .rst_counter(rst_counter),
       .inc_counter(inc_counter),
       .busy(busy),
       .write_en(write_en),
       .counter_gt_max(counter_gt_max),
       .counter_gt_buflen(counter_gt_buflen),
       .clk(clk),
       .rst(rst)
   );
   
   flush_datapath #(
      .CLOCK_FREQ(CLOCK_FREQ),
      .NUM_NEOPIXELS(NUM_NEOPIXELS),
      .NEOPIXEL_RESET_DELAY(NEOPIXEL_RESET_DELAY)
   ) flush_datapath_instance (
      .dout(dout),
      .busy(busy),
      .counter_gt_buflen(counter_gt_buflen),
      .counter_gt_max(counter_gt_max),
      .brg(brg),
      .latch(latch),
      .rst_counter(rst_counter),
      .inc_counter(inc_counter),
      .send(send),
      .clk(clk),
      .rst(rst)
   );
   
endmodule
